import unittest
from classes.dummy import Dummy

class DummyTest(unittest.TestCase):
	def test_execute(self):
		dummy = Dummy()
		self.assertEqual(dummy.execute(), True)

if __name__ == '__main__':
	    unittest.main()
