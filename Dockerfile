FROM debian

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y python-pip

RUN pip install mock
RUN pip install magicmock

COPY . /var/www/   
